#!/usr/bin/env python3

inventario = {}


def insertar(codigo: str, nombre: str, precio: float, cantidad: int):
    # inventario[codigo] = [nombre, precio, cantidad]
    valores = {"nombre": nombre, "precio": precio, "cantidad": cantidad}
    inventario[codigo] = valores


def listar():
    for codigo, valores in inventario.items():
        print(f"Codigo: {codigo}, Nombre: {valores['nombre']}, Precio: {valores['precio']}, Cantidad: {valores['cantidad']}")

def consultar(codigo: str):
    if codigo in inventario:
        valores = inventario[codigo]
        print(f"Codigo: {codigo}, Nombre: {valores['nombre']}, Precio: {valores['precio']}, Cantidad: {valores['cantidad']}")
    else:
        print('Articulo no encontrado en el inventario')

def agotados():
    for codigo, valores in inventario.items():
        if valores['cantidad'] == 0:
            print(f"Artículo agotado - Código: {codigo}, Nombre: {valores['nombre']}, Precio: {valores['precio']}")

def pide_articulo() -> (str, str, float, int):
    codigo = input('Ingrese el codigo del articulo: ')
    nombre = input("Ingrese el nombre del artículo: ")
    precio = float(input("Ingrese el precio del artículo: "))
    cantidad = int(input("Ingrese la cantidad del artículo: "))
    return codigo, nombre, precio, cantidad


def menu() -> int:
    print("1. Insertar un artículo")
    print("2. Listar artículos")
    print("3. Consultar artículo")
    print("4. Artículos agotados")
    print("0. Salir")
    opcion = input("Opción: ")
    return opcion


def main():
    seguir = True
    while seguir:
        opcion = menu()
        if opcion == '0':
            seguir = False
        elif opcion == '1':
            codigo, nombre, precio, cantidad = pide_articulo()
            insertar(codigo, nombre, precio, cantidad)
        elif opcion == '2':
            listar()
        elif opcion == '3':
            codigo = input('ingrese el codigo del articulo a consultar: ')
            consultar(codigo)
        elif opcion == '4':
            agotados()
        else:
            print("Opción incorrecta")


if __name__ == '__main__':
    main()
